from math import sqrt

class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def DistanceTo(self, x, y):
        return sqrt((self.x - x) ** 2 + (self.y -y) ** 2)

    def VectorTo(self, position2):
        return (position2.x - self.x, position2.y - self.y)

    def __subtraction__(self, pos2):
        return self.DistanceTo(pos2.x, pos2.y)