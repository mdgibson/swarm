from math import atan2, pi

tau = 2 * pi

def angle_between(x1,y1,x2,y2):
    deltaX = x2 - x1
    deltaY = y2 - y1
    angle = atan2(deltaY, deltaX)
    return normalize_angle(angle)

def normalize_angle(theta): 
    theta = theta if theta < tau else theta - tau
    return theta if theta >= 0 else theta + tau