from .Util import normalize_angle

class ViewCone:
    def __init__(self, *args, **kwargs):
        viewing_angle = kwargs.get('viewing_angle', None)
        if viewing_angle is not None:
            self.viewing_angle = viewing_angle
            return
        
        orientation = kwargs.get('orientation', None)
        sweep = kwargs.get('sweep', None)
        if orientation is not None:
            if sweep is None:
                raise Exception("Argument error: sweep is required if orientation is provided")
            else:
                min_theta = normalize_angle(orientation - sweep)
                max_theta = normalize_angle(orientation + sweep)
                self.viewing_angle = (orientation - sweep, orientation + sweep)
                if (max_theta < min_theta):
                    self.viewing_angle = self.viewing_angle[::-1]

        if viewing_angle is None and orientation is None:
            raise Exception("Argument error: Must supply either viewing_angle or orientation/sweep")
    
    def __eq__(self, other):
        return self.viewing_angle == other.viewing_angle

    def __ne__(self,other):
        return not self.__eq__(other)