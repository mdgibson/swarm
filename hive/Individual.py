import threading
from .Detectable import Detectable

class Individual (threading.Thread, Detectable):
    sensors = []
    goal = []

    def __init__(self):
        threading.Thread.__init__(self)
        self.parse_rules()

    def run(self):
        pass

    def parse_rules(self):
        pass

    def detect_surroundings(self, detectables):
        """Iterate through entities attempting detection using sensors.
        
        Keyword arguments:
        entities -- the entities to attempt to detect

        Class variables used:
        sensors -- Attempts detection of entity using all available sensors
        """
        for sensor in self.sensors:
            for detectable in detectables:
                if detectable.detect_and_occlude(sensor):
                    self.respond_to_detection(detectable)

    def entity_detected(self, entity, sensor):
        """Determine if entity is detected by a sensor and return bool."""
        return True

    def respond_to_detection(self, detection):
        pass