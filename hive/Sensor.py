from .ViewCone import ViewCone
from .Position import Position


class Sensor:
    def __init__(self, position, view_cone):
        self.position = position
        self._original_view_angle = view_cone.viewing_angle
        self.view_cones = [view_cone]

    def __eq__(self, other):
        return self.view_cones == other.view_cones

    def __ne__(self,other):
        return not self.__eq__(other)

    def reinitialize(self):
        self.view_cones = [ViewCone(viewing_angle=self._original_view_angle)]