from .Util import angle_between
from .ViewCone import ViewCone
from .Visibility import Visibility
import random

class Detectable:
    """Base class representing an entity which is detectable by sensors

    Describes the position, size, and visibility of an entity. All Detectables
    are rectangular prisms.

    Intended for subclassing and provides interfact for detection of an entity
    by Sensors.
    """
    def __init__(self, position,
        sizeX, sizeY,
        visibility):
        """Initialize Detectable with given position, size, and visibility

        Keyword arguments:
        position -- position of the center of the object
        size -- total size of the object
        visibility -- Visibility object recording ease of detection by various
            means
        """
        self.position = position
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.visibility = visibility

    def occlude_cone(self, sensor_position, view_cone):
        # See if sensor will detect us
        if random.random() > 1:
            return False
        
        # See if sensor sees us
        a = angle_between(
            sensor_position.x,
            sensor_position.y,
            self.position.x - self.sizeX / 2,
            self.position.y + self.sizeY / 2
            )
        b = angle_between(
            sensor_position.x,
            sensor_position.y,
            self.position.x + self.sizeX / 2,
            self.position.y + self.sizeY / 2
            )
        c = angle_between(
            sensor_position.x,
            sensor_position.y,
            self.position.x + self.sizeX / 2,
            self.position.y - self.sizeY / 2
            )
        d = angle_between(
            sensor_position.x,
            sensor_position.y,
            self.position.x - self.sizeX / 2,
            self.position.y - self.sizeY / 2
            )

        phi_min = min([abs(a), abs(b), abs(c), abs(d)])
        phi_max = max([abs(a), abs(b), abs(c), abs(d)])

        viewing_angles = []
        if (phi_min < view_cone.viewing_angle[0] and phi_max > view_cone.viewing_angle[1]):
            return True, []
        if (view_cone.viewing_angle[0] < phi_min < view_cone.viewing_angle[1]):
            viewing_angles.append(ViewCone(viewing_angle=(view_cone.viewing_angle[0], phi_min)))
        if (view_cone.viewing_angle[0] < phi_max < view_cone.viewing_angle[1]):
            viewing_angles.append(ViewCone(viewing_angle=(phi_max, view_cone.viewing_angle[1])))

        return viewing_angles != [], viewing_angles
            
    def detect_and_occlude(self, sensor):
        cones = []
        occluded = False
        for view_cone in sensor.view_cones:
            detected, occluded_cones = self.occlude_cone(sensor.position, view_cone)
            occluded |= detected
            cones += occluded_cones
        
        sensor.view_cones = cones
        return occluded
        