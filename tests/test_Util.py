from ..hive.Util import angle_between
import math

def test_angle_between():
    assert angle_between(0,0,0,0) == 0
    assert angle_between(0,0,0,1) == math.pi / 2
    assert angle_between(0,0,-1,1) == 3 * math.pi / 4
    assert angle_between(0,0,-1,-1) == - 3 * math.pi / 4 + 2 * math.pi
    assert angle_between(0,0,1,-1) == - math.pi / 4 + 2 * math.pi