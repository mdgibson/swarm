from ..hive.ViewCone import ViewCone
from ..hive.Sensor import Sensor
from ..hive.Position import Position

def test_sensor_reinitializaton():
    viewcone = ViewCone(viewing_angle=(0,180))
    sensor = Sensor(Position(0,0), viewcone)
    sensor.view_cones = []

    sensor.reinitialize()
    assert sensor.view_cones == [viewcone]