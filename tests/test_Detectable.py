from ..hive import Position, Detectable, Visibility, ViewCone, Sensor
from math import pi

vis = Visibility()
pos = Position(0.5,0.5)
detectable = Detectable(pos, 1, 1, vis)

def test_attributes():
    assert hasattr(detectable, 'position')
    assert detectable.position == pos
    assert hasattr(detectable, 'sizeX')
    assert detectable.sizeX == 1
    assert hasattr(detectable, 'sizeY')
    assert detectable.sizeY == 1
    assert hasattr(detectable, 'visibility')
    assert detectable.visibility == vis

def test_trivial_full_occlusion_of_sensor():
    # detectable covers (0,0) to (1,1)
    sensor = Sensor(Position(0,0), ViewCone(orientation=0, sweep= pi / 4))
    detected = detectable.detect_and_occlude(sensor)
    
    assert detected == True
    assert sensor.view_cones == []

def test_full_occlusion_of_sensor():
    sensor = Sensor(Position(2,0.5), ViewCone(orientation=pi, sweep=pi / 7))
    detected = detectable.detect_and_occlude(sensor)

    assert detected == True
    assert sensor.view_cones == []

def test_occlude_low_theta():
    sensor = Sensor(Position(2,0), ViewCone(orientation=pi, sweep=pi/12))
    detected = detectable.detect_and_occlude(sensor)

    assert detected == True
    expected_view_cones = [ViewCone(viewing_angle=(pi, pi + pi/12))]
    assert len(sensor.view_cones) == len(expected_view_cones)
    for actual, expected in zip(sensor.view_cones, expected_view_cones):
        assert actual.viewing_angle == expected.viewing_angle

def test_occlude_high_theta():
    sensor = Sensor(Position(2,1), ViewCone(orientation=pi, sweep=pi/12))
    detected = detectable.detect_and_occlude(sensor)

    assert detected == True
    expected_view_cones = [ViewCone(viewing_angle=(pi-pi/12,pi))]
    assert len(sensor.view_cones) == len(expected_view_cones)
    for actual, expected in zip(sensor.view_cones, expected_view_cones):
        assert actual.viewing_angle == expected.viewing_angle

# def test_partial_occlusion_of_sensor_cone():
#     sensor = ViewCone()

